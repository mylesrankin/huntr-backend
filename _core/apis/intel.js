const express = require('express');
const cors = require('cors');

const serverless = require('serverless-http');

const jwt = require('jsonwebtoken');

const esi = require('../controllers/esi');
const character = require('../controllers/character');
const user = require('../controllers/user');

const { permissionMiddleware } = require('./permissionMiddleware');

const app = express();
app.use(cors({ origin: true }));
app.use(express.json());
app.use(permissionMiddleware);

app.post('/api/intel', async (req, res) => {});

// app.post('/api/sso/logout', (req, res) => {
//   res.status(200).send();
// });

module.exports.handler = serverless(app);
module.exports.app = app;
