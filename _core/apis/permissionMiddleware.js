const user = require('../controllers/user');
const jwt = require('jsonwebtoken');

const permissionedRoutes = {
  //   GET: {
  //     '/api/user': ['registered'],
  //   },
};

const checkPermissions = (user, route) => {
  return user.some((r) => route.indexOf(r) >= 0);
};

const get = (p, o) => p.reduce((xs, x) => (xs && xs[x] ? xs[x] : null), o);

const permissionMiddleware = async (req, res, next) => {
  try {
    console.log('___MIDDLEWARE___');
    console.log(req.url);
    console.log(req.method);
    console.log(get([req.method, req.url], permissionedRoutes));
    if (get([req.method, req.url], permissionedRoutes)) {
      let routePerms = permissionedRoutes[req.method][req.url];
      console.log('Found in perms dict');
      console.log(permissionedRoutes[req.method][req.url]);
      console.log(req.headers.authorization);
      if (req.headers.authorization) {
        console.log('in auth flow');
        let authToken = req.headers.authorization.split(' ')[1];
        let tokenContents = jwt.verify(authToken, process.env.JWT_PRIVATE_KEY);
        let userProfile = await user.getUser(tokenContents.user);
        console.log(userProfile);
        console.log('getting here');
        if (userProfile) {
          console.log('hitting 3');
          res.user = userProfile;
          if (checkPermissions(userProfile.permissions, routePerms)) {
            console.log('authed');
            next();
          } else {
            console.log('not authed');
            res
              .status(401)
              .send('You do not have the permissions to access this resource');
          }
        } else {
          console.log('hitting 1');
          res.status(401).send('The user with the id provided does not exist');
        }
      } else {
        console.log('hitting 2');
        res.status(400).send('No authorization header provided');
      }
    } else {
      console.log('skipping perms middleware');
      next();
    }
    console.log('__END OF MIDDLEWARE___');
  } catch (err) {
    console.log('HITTING 500 ERROR');
    console.error(err);
    res.status(500).send('Internal Server Error');
  }
};

module.exports = { permissionMiddleware };
