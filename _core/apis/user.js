const express = require('express');
const serverless = require('serverless-http');
const cors = require('cors');

const jwt = require('jsonwebtoken');

const esi = require('../controllers/esi');
const character = require('../controllers/character');
const user = require('../controllers/user');

const { permissionMiddleware } = require('./permissionMiddleware');

const app = express();
app.use(express.json());
app.use(cors({ origin: true }));
app.use(permissionMiddleware);

app.get('/api/user', async (req, res) => {
  res.set({
    'Access-Control-Allow-Origin': '*', // Required for CORS support to work
    'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
  });
  try {
    console.log('hit /api/user');
    console.log(req.headers);
    if (req.headers.authorization) {
      console.log(req.headers.authorization);
      let authToken = req.headers.authorization.split(' ')[1];
      console.log(authToken);
      let tokenContents = jwt.verify(authToken, process.env.JWT_PRIVATE_KEY);
      console.log(tokenContents);
      let userProfile = await user.getUser(tokenContents.user);
      let userCharacters = await character.getUserCharacters(
        tokenContents.user
      );
      if (userProfile) {
        delete userProfile._doc.mainCharacter.credentials;
        res.set({
          'Access-Control-Allow-Origin': '*', // Required for CORS support to work
          'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
        });
        res
          .status(200)
          .send({ ...userProfile._doc, characters: userCharacters });
      } else {
        res.status(401).send('The user id provided does not exist');
      }
    } else {
      res.status(400).send('No authorization header provided');
    }
  } catch (err) {
    console.log('THIS ERR');
    console.log(err);
    res.status(401).send('Invalid/Unauthorised ');
  }
});

// app.post('/api/sso/logout', (req, res) => {
//   res.status(200).send();
// });

module.exports.handler = serverless(app);
module.exports.app = app;
