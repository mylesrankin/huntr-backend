const serverless = require('serverless-http');

const express = require('express');
var cors = require('cors');

const jwt = require('jsonwebtoken');

const esi = require('../controllers/esi');
const character = require('../controllers/character');
const user = require('../controllers/user');

const app = express();
app.use(cors());

app.use(express.json());

// app.options('/api/sso/login', cors());

app.post('/api/sso/login', async (req, res) => {
  // res.set({
  //   'Access-Control-Allow-Origin': '*', // Required for CORS support to work
  //   'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
  // });
  if (req.body.code) {
    try {
      console.log('0');
      let currentUser;
      console.log(req.body.code);
      console.log('1');
      let tokens = await esi.swapCodeForTokens(req.body.code);
      console.log('2');
      let verify = await esi.verifyCreds(tokens);
      console.log('3');
      console.log(verify);
      console.log('4');
      let checkCharacter = await character.getCharacter(verify.CharacterID);
      if (checkCharacter) {
        // already exists, update authcreds for that user and give session
        await character.updateCredentials(verify.CharacterID, tokens);
        currentUser = checkCharacter.user;
        console.log('5');
      } else {
        // does not exist, create character and user.
        let newUser = await user.addUser(verify.CharacterID);
        await character.create(newUser._id, verify, tokens);
        currentUser = newUser;
        console.log('6');
      }
      let JWTPayload = {
        user: currentUser._id,
        permissions: currentUser.permissions,
        mainCharacter: currentUser.mainCharacter,
      };
      let loginToken = await jwt.sign(JWTPayload, process.env.JWT_PRIVATE_KEY);
      res.status(200).json({ Authorization: 'Bearer ' + loginToken });
    } catch (err) {
      console.log('ERRORING HERE?');
      console.error(err);
      res.status(400).send(err);
    }
  } else {
    res.status(403).send('No authorization code provided');
  }
});

// app.post('/api/sso/logout', (req, res) => {
//   res.status(200).send();
// });

module.exports.handler = serverless(app);
module.exports.app = app;
