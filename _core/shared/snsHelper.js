var AWS = require('aws-sdk');

const publishToSNS = (Payload, Topic, MessageAttributes) => {
  return new Promise(async (resolve, reject) => {
    let TopicArn = `arn:aws:sns:${process.env.region}:${process.env.accountId}:${process.env.STAGE}${Topic}`;

    var params = {
      Message: JSON.stringify(Payload) /* required */,
      TopicArn: TopicArn,
      MessageAttributes: MessageAttributes,
    };

    var publishTextPromise = new AWS.SNS({ apiVersion: '2010-03-31' })
      .publish(params)
      .promise();

    publishTextPromise
      .then((data) => {
        console.log(
          `Message ${params.Message} send sent to the topic ${params.TopicArn}`
        );
        resolve();
      })
      .catch((err) => {
        console.error(err, err.stack);
        reject(err);
      });
  });
};

module.exports = { publishToSNS };
