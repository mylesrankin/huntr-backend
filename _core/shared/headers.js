const ESI_CLIENT_ID =
  process.env.ESI_CLIENT_ID || 'e592866ecb3e441e8e30ddb22d48a132';
const ESI_SECRET_KEY =
  process.env.ESI_SECRET_KEY || 'nQvnhppXbqiFoqdOvxrPHboG0sY9EP06dKGYx8Sq';

const getSecureHeaders = (contentType) => {
  let basicAuth = new Buffer(`${ESI_CLIENT_ID}:${ESI_SECRET_KEY}`);
  return {
    Authorization: 'Basic ' + basicAuth.toString('base64'),
    'Content-Type': contentType ? contentType : 'application/json',
  };
};

module.exports = { getSecureHeaders };
