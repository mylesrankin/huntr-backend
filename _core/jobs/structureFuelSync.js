const structuresController = require('../controllers/structures');
const esi = require('../controllers/esi');

function handler(event, context, cb) {
  context.callbackWaitsForEmptyEventLoop = false;

  event.Records.map((item) => {
    console.info(item);
    let payload = JSON.parse(item.Sns.Message);
    action(payload).then(
      (resolvedMessage) => {
        cb(null, resolvedMessage);
      },
      (rejectedMessage) => {
        console.error(rejectedMessage);
        cb(rejectedMessage);
      }
    );
  });
}

const action = async (payload) => {
  try {
    await processFuelSyncForCharacter(payload.character);
    return Promise.resolve();
  } catch (err) {
    console.error(err);
    return Promise.reject(err);
  }
};

const processFuelSyncForCharacter = async (character) => {
  try {
    console.log('process structures fuel');
    let structures = await esi.getCharacterStructures(character);
    console.log(structures);
    if (structures.length > 0) {
      await structuresController.bulkUpdateOrInsert(
        structures,
        character.characterId
      );
    }
    return Promise.resolve();
  } catch (err) {
    return Promise.reject(err);
  }
};

module.exports.handler = handler;
