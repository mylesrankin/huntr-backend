const extractionsController = require('../controllers/moonExtractions');
const esi = require('../controllers/esi');

function handler(event, context, cb) {
  context.callbackWaitsForEmptyEventLoop = false;

  event.Records.map((item) => {
    console.info(item);
    let payload = JSON.parse(item.Sns.Message);
    action(payload).then(
      (resolvedMessage) => {
        cb(null, resolvedMessage);
      },
      (rejectedMessage) => {
        console.error(rejectedMessage);
        cb(rejectedMessage);
      }
    );
  });
}

const action = async (payload) => {
  try {
    console.log(payload);
    await processExtractionSyncForCharacter(payload.character);
    return Promise.resolve();
  } catch (err) {
    console.error(err);
    return Promise.reject(err);
  }
};

const processExtractionSyncForCharacter = async (character) => {
  try {
    console.log('1');
    let extractions = await esi.getCharacterExtractions(character);
    console.log(extractions);
    console.log('2');
    if (extractions.length > 0) {
      console.log('a1');
      await extractionsController.bulkUpdateOrInsert(
        extractions,
        character.characterId
      );
      console.log('a2');
    }
    console.log('3');
    return Promise.resolve();
  } catch (err) {
    console.log(err);
    return Promise.reject(err);
  }
};

module.exports.handler = handler;
