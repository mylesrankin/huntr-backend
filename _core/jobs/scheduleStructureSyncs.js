const characterController = require('../controllers/character');

const publishToSNS = require('../shared/snsHelper').publishToSNS;

const esi = require('../controllers/esi');

function handler(event, context, cb) {
  context.callbackWaitsForEmptyEventLoop = false;

  action().then(
    (resolvedMessage) => {
      cb(null, resolvedMessage);
    },
    (rejectedMessage) => {
      console.error(rejectedMessage);
      cb(rejectedMessage);
    }
  );
}

const action = async () => {
  try {
    // get characters
    let charactersToSync = await characterController.getStructureSyncCharacters();
    console.log(`There are ${charactersToSync.length} characters to sync`);
    console.log(charactersToSync);
    if (charactersToSync.length > 0) {
      let promises = charactersToSync.map((character) =>
        publishToSNS({ character }, '-structure-topic', {
          type: {
            DataType: 'String',
            StringValue: 'syncCharacter',
          },
        })
      );
      return Promise.all(promises);
    } else {
      return Promise.resolve();
    }
  } catch (err) {
    console.error(err);
    return Promise.reject(err);
  }
};

module.exports.handler = handler;
