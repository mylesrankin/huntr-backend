var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');

var Schema = mongoose.Schema;

var characterSchema = new Schema({
  _id: { type: Number, required: true },
  user: { type: Schema.Types.ObjectId, ref: 'users', required: true },
  name: { type: String, required: true },
  characterId: { type: Number, required: true },
  credentials: { type: Schema.Types.Mixed },
  linkDate: { type: Date, default: moment() },
  syncStructures: { type: Boolean, default: false },
});

module.exports = mongoose.model('characters', characterSchema);
