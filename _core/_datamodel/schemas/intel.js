var mongoose = require('mongoose');
var moment = require('moment');

var Schema = mongoose.Schema;

var intelSchema = new Schema({
  user: {
    type: Schema.Types.Mixed,
    ref: 'users',
    required: true,
  },
  type: { type: String, required: true },
  character: { type: String, required: true },
  locatorType: { type: String },
  location: { type: String },
  timestamp: { type: Date, default: moment() },
});

module.exports = mongoose.model('intel', intelSchema);
