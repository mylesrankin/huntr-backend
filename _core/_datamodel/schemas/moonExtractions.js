var mongoose = require('mongoose');
var moment = require('moment');

var Schema = mongoose.Schema;

var moonExtractionsSchema = new Schema({
  _id: { type: Number }, // this will use moon id
  character: { type: String, required: true },
  extractionData: {
    chunk_arrival_time: { type: Date },
    extraction_start_time: { type: Date },
    moon_id: { type: Number },
    natural_decay_time: { type: Date },
    structure_id: { type: Number, ref: 'structures' },
  },
});

module.exports = mongoose.model('moonExtractions', moonExtractionsSchema);
