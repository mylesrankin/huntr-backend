var mongoose = require('mongoose');
var moment = require('moment');

var Schema = mongoose.Schema;

var structuresSchema = new Schema({
  _id: { type: Number, required: true }, // this will use structure_id
  character: { type: String, required: true },
  structureData: {
    corporation_id: { type: Number },
    fuel_expires: { type: Date },
    profile_id: { type: Number },
    reinforce_hour: { type: Number },
    services: { type: Array },
    state: { type: String },
    structure_id: { type: Number },
    system_id: { type: Number },
    type_id: { type: Number },
  },
});

module.exports = mongoose.model('structures', structuresSchema);
