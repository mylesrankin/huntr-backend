var mongoose = require('mongoose');
var moment = require('moment');

var Schema = mongoose.Schema;

var userSchema = new Schema({
  mainCharacter: {
    type: Schema.Types.Mixed,
    ref: 'characters',
    required: true,
  },
  permissions: { type: Array, default: ['registered'] },
  created: { type: Date, default: moment() },
});

module.exports = mongoose.model('users', userSchema);
