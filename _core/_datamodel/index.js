var mongoose = require('mongoose');
require('dotenv').config();

process.env.DB_CONNECTION_STRING &&
  createConnection(process.env.DB_CONNECTION_STRING);
mongoose.Promise = global.Promise;

function dbDisconnect() {
  mongoose.connection.close().then(
    (success) => {
      console.log('Connection closed');
    },
    (err) => {
      console.log('Failed to close connection');
      console.log('Connection Close Error: ', err);
    }
  );
}

function createConnection(connectionString) {
  mongoose.Promise = global.Promise;
  mongoose.connect(connectionString, {
    useNewUrlParser: true,
    useFindAndModify: false,
  });

  var db = mongoose.connection;
  db.on('error', function () {
    console.error.bind(console, 'connection error:');
    mongoose.disconnect();
  });
  db.on('disconnected', function () {
    console.log('db disconnected..no retry');
  });

  db.once('open', function () {
    console.log('db connected: ' + process.env.DB_CONNECTION_STRING);
  });
  return db;
}

exports.createConnection = createConnection;
exports.dbDisconnect = dbDisconnect;
exports.characterModel = require('./schemas/character');
exports.userModel = require('./schemas/user');
exports.intelModel = require('./schemas/intel');
exports.structureModel = require('./schemas/structures');
exports.moonExtractionModel = require('./schemas/moonExtractions');

// exports.killmailModel = require('./schemas/killmail');
