const fetch = require('node-fetch');

const character = require('./character');
const user = require('./user');

const { getSecureHeaders } = require('../shared/headers');

const verifyCreds = (creds) => {
  console.log(creds);
  return new Promise((resolve, reject) => {
    fetch('https://login.eveonline.com/oauth/verify', {
      method: 'get',
      headers: { Authorization: 'Bearer ' + creds.access_token },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.error) reject(res);
        else resolve(res);
      })
      .catch((err) => reject(err));
  });
};

const swapCodeForTokens = (code) => {
  return new Promise((resolve, reject) => {
    fetch('https://login.eveonline.com/oauth/token', {
      method: 'post',
      body: JSON.stringify({ grant_type: 'authorization_code', code: code }),
      headers: getSecureHeaders(),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('test1');
        console.log(res);
        if (res.error) reject(res);
        else resolve(res);
      })
      .catch((err) => {
        console.log('test2');
        reject(err);
      });
  });
};

const refreshTokens = (characterId, credentials) => {
  return new Promise((resolve, reject) => {
    fetch('https://login.eveonline.com/v2/oauth/token', {
      method: 'post',
      body: JSON.stringify({
        grant_type: 'refresh_token',
        refresh_token: credentials.refresh_token,
      }),
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'Basic ZTU5Mjg2NmVjYjNlNDQxZThlMzBkZGIyMmQ0OGExMzI6blF2bmhwcFhicWlGb3FkT3Z4clBIYm9HMHNZOUVQMDZkS0dZeDhTcQ==',
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log('[Inside refreshTokens(...)] New creds (res): ' + res);
        if (!res.error) {
          return character.updateCredentials(characterId, res);
        } else {
          console.log(res);
          return new Error('Error when refreshing: ' + res.error);
        }
      })
      .then((res) => resolve(res))
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
};

// const refreshTokens = async (characterId, credentials) => {
//   try {
//     let res = await fetch('https://login.eveonline.com/v2/oauth/token', {
//       method: 'post',
//       body: JSON.stringify({
//         grant_type: 'refresh_token',
//         refresh_token: credentials.refresh_token,
//       }),
//       headers: {
//         'Content-Type': 'application/json',
//         Authorization:
//           'Basic ZTU5Mjg2NmVjYjNlNDQxZThlMzBkZGIyMmQ0OGExMzI6blF2bmhwcFhicWlGb3FkT3Z4clBIYm9HMHNZOUVQMDZkS0dZeDhTcQ==',
//       },
//     });
//     res = await res.json();
//     console.log(res);
//     await character.updateCredentials(characterId, res);

//     return Promise.resolve(res);
//   } catch (error) {
//     console.log(error);
//     return Promise.reject(error);
//   }
// };

const queryold = (route, method, character, authentication = true) => {
  return new Promise((resolve, reject) => {
    let options = {
      method: method,
      headers: {
        'Content-Type': 'application/json',
      },
    };

    if (authentication) {
      options.headers.authorization = `Bearer ${character.credentials.access_token}`;
    }

    console.log(`Let's check those options, shall we?`);
    console.log(options);
    fetch(route, options)
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        if (res.sso_status === 401) {
          console.log('need to refresh');
          console.log(res.sso_status);
          refreshTokens(character.characterId, character.credentials)
            .then((newCreds) => {
              console.log('new creds: ' + newCreds);
              character.credentials = newCreds;
              query(route, method, character, authentication);
            })
            .catch((err) => {
              console.log('erroring here');
              console.log(err);
              reject(err);
            });
        } else {
          return res;
        }
      })
      .then((res) => resolve(res))
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
};

const query = async (route, method, character, authentication = true) => {
  try {
    let options = {
      method: method,
      headers: {
        'Content-Type': 'application/json',
      },
    };

    if (authentication) {
      options.headers.authorization = `Bearer ${character.credentials.access_token}`;
    }

    console.log(`Let's check those options, shall we?`);
    console.log(options);

    console.log(`Now querying: ${options.method} - ${route}`);
    let res = await fetch(route, options);
    res = await res.json();

    console.log(res);
    console.log('Status: ' + res.sso_status);

    if (res.sso_status === 401 || res.error === 'token is expired') {
      console.log('need to refresh');
      let refreshedCreds = await refreshTokens(
        character.characterId,
        character.credentials
      );
      console.log('New creds: ' + refreshedCreds);
      character.credentials = refreshedCreds.credentials;
      res = await query(route, method, character, authentication);
    }
    return Promise.resolve(res);
  } catch (err) {
    console.log(err);
    return Promise.reject(err);
  }
};

// query(
//   'https://esi.evetech.net/latest/corporations/98598059/structures',
//   {
//     method: 'GET',
//     headers: {
//       Authorization:
//         'Bearer 1|CfDJ8Hj9X4L/huFJpslTkv3swZPppOdLbdnR9mSbdsJqBZbnf8Y2rM1GGFbkErdpJrE5JhF10pkboWVa95MykYItvZrdX7bK85u0dvpGInhOm3XIDuIp9h10+TME/ALWFDROuSBE0X1q+hkrC5XL1bcLQYSuUUlp+NDSBIPb1HKV9D3c',
//     },
//   },
//   {
//     characterId: 927451403,
//     credentials: {
//       access_token:
//         '1|CfDJ8Hj9X4L/huFJpslTkv3swZPppOdLbdnR9mSbdsJqBZbnf8Y2rM1GGFbkErdpJrE5JhF10pkboWVa95MykYItvZrdX7bK85u0dvpGInhOm3XIDuIp9h10+TME/ALWFDROuSBE0X1q+hkrC5XL1bcLQYSuUUlp+NDSBIPb1HKV9D3c',
//       refresh_token:
//         'QE_58AzrznGwoOgdHvz2znOMA0kKy3q2gI_7mYe-PkgX15UivrrPFHukp-vjR3j2FNtDkFX-S1yEmQQkRxFr4Q',
//     },
//   }
// )
//   .then((res) => console.log(res))
//   .catch((err) => console.log(err));

const getCharacterCorporation = (characterId) => {
  return new Promise((resolve, reject) => {
    query(
      `https://esi.evetech.net/latest/characters/${characterId}`,
      'GET',
      character,
      false
    )
      .then((res) => {
        console.log(res);
        return res;
      })
      .then((res) => resolve(res.corporation_id))
      .catch((err) => reject(err));
  });
};

const getCharacterStructures = async (characterObj) => {
  try {
    let characterCorporationId = await getCharacterCorporation(
      characterObj.characterId
    );
    let structures = await query(
      `https://esi.evetech.net/latest/corporations/${characterCorporationId}/structures`,
      'GET',
      characterObj
    );
    return Promise.resolve(structures);
  } catch (err) {
    return Promise.reject(err);
  }
};

const getCharacterExtractions = async (characterObj) => {
  try {
    console.log(characterObj);
    let characterCorporationId = await getCharacterCorporation(
      characterObj.characterId
    );
    console.log('see below');
    console.log(characterCorporationId);
    console.log('here is the char obj');
    console.log(characterObj);
    let extractions = await query(
      `https://esi.evetech.net/latest/corporation/${characterCorporationId}/mining/extractions/`,
      'GET',
      characterObj,
      true
    );
    console.log('extractions');
    console.log(extractions);
    return Promise.resolve(extractions);
  } catch (err) {
    return Promise.reject(err);
  }
};

module.exports = {
  swapCodeForTokens,
  verifyCreds,
  refreshTokens,
  getCharacterCorporation,
  getCharacterStructures,
  getCharacterExtractions,
};
