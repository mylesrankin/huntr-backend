const intelModel = require('../_datamodel').intelModel;
const { ObjectId } = require('mongodb');

const addIntel = (user, type, character, locatorType, location) => {
  console.log(mainCharacterId);
  return new Promise((resolve, reject) => {
    intelModel
      .create({
        ...user,
        ...type,
        ...character,
        ...(locatorType && locatorType),
        ...(location && location),
      })
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

// const getUser = (id) => {
//   return new Promise((resolve, reject) => {
//     userModel
//       .findOne({ _id: id })
//       .populate('mainCharacter')
//       .then((res) => resolve(res))
//       .catch((err) => reject(err));
//   });
// };

const getCharacterIntel = (characterId, type) => {
  return new Promise((resolve, reject) => {
    intelModel.find({ character: characterId, type: type });
  });
};

module.exports = { addIntel };
