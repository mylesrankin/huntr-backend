const structureModel = require('../_datamodel').structureModel;

const bulkUpdateOrInsert = (structuresResult, characterId) => {
  return new Promise((resolve, reject) => {
    Promise.all(
      structuresResult.map((structure) => {
        return structureModel.findOneAndUpdate(
          { _id: structure.structure_id },
          {
            $set: {
              character: characterId,
              structureData: structure,
            },
          },
          { upsert: true, new: true }
        );
      })
    )
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

module.exports = { bulkUpdateOrInsert };
