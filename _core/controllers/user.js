const userModel = require('../_datamodel').userModel;
const { ObjectId } = require('mongodb');

const addUser = (mainCharacterId) => {
  console.log(mainCharacterId);
  return new Promise((resolve, reject) => {
    userModel
      .create({
        mainCharacter: mainCharacterId,
      })
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

const getUser = (id) => {
  return new Promise((resolve, reject) => {
    userModel
      .findOne({ _id: id })
      .populate('mainCharacter')
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

// const checkExists = () => {
//   return new Promise((resolve, reject) => {
//     userModel.findOne({});
//   });
// };

module.exports = { getUser, addUser };
