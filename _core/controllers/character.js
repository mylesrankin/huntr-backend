const characterModel = require('../_datamodel').characterModel;

const create = (userId, character, credentials) => {
  return new Promise((resolve, reject) => {
    characterModel
      .create({
        _id: character.CharacterID,
        user: userId,
        name: character.CharacterName,
        characterId: character.CharacterID,
        credentials,
      })
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

const updateCredentials = (characterId, credentials) => {
  return new Promise((resolve, reject) => {
    let { access_token, token_type, expires_in, refresh_token } = credentials;
    if (access_token && token_type && expires_in && refresh_token) {
      characterModel
        .findOneAndUpdate({ characterId }, { $set: { credentials } })
        .then((res) => resolve(res))
        .catch((err) => reject(err));
    } else {
      reject('Missing credentials');
    }
  });
};

const remove = (characterId) => {
  return new Promise((resolve, reject) => {
    characterModel
      .findOneAndDelete({ characterId })
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

const getCharacter = (characterId) => {
  return new Promise((resolve, reject) => {
    characterModel
      .findOne({ characterId })
      .populate('user')
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

const getUserCharacters = (userId) => {
  return new Promise((resolve, reject) => {
    characterModel
      .find({ user: userId }, { credentials: 0 })
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

const getStructureSyncCharacters = () => {
  return new Promise((resolve, reject) => {
    characterModel
      .find({ syncStructures: true })
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

module.exports = {
  updateCredentials,
  create,
  remove,
  getCharacter,
  getUserCharacters,
  getStructureSyncCharacters,
};
