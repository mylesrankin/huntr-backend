const moonExtractionModel = require('../_datamodel').moonExtractionModel;

const bulkUpdateOrInsert = (extractionsResult, characterId) => {
  return new Promise((resolve, reject) => {
    Promise.all(
      extractionsResult.map((extraction) => {
        return moonExtractionModel.findOneAndUpdate(
          { _id: extraction.moon_id },
          {
            $set: {
              character: characterId,
              extractionData: extraction,
            },
          },
          { upsert: true, new: true }
        );
      })
    )
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

module.exports = { bulkUpdateOrInsert };
